public class User {
    private String username;
    private String password;
    private userType user;

    public User (String username, String password, userType user) {
        this.username=username;
        this.password=password;
        this.user=user;
    }

    public String getUserName() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }
    public userType getUser() {
        return this.user;
    }
}